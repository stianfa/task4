﻿using System;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {

            /* Example square (7x8):
                    ########
                    #      #
                    # #### #
                    # #  # # 
                    # #### #
                    #      #
                    ########
            */

            #region input

            // Collect and parse height and width of the rectangle
            Console.WriteLine("Please enter the height of the rectangle:");
            int height = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Please enter the width of the rectangle:");
            int width = Convert.ToInt32(Console.ReadLine());

            #endregion

            // Iterate through the coordinates and draw the figure
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (i == 0 || i == height - 1 || j == 0 || j == width - 1)    // Outer border
                    {
                        Console.Write("#");
                    }
                    else if(i == 2 && j > 1 && j < width-2)                       // Inner upper border 
                    {
                        Console.Write("#");
                    }
                    else if(i == height-3 && j > 1 && j < width - 2)              // Inner lower border
                    {
                        Console.Write("#");
                    }
                    else if (j == 2 && i > 1 && i < height -2)                    // Inner left border
                    {
                        Console.Write("#");
                    }
                    else if (j == width-3 && i > 1 && i < height - 2)             // Inner right border
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write(" ");                                      
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
